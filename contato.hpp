#ifndef CONTATO_H
#define CONTATO_H

#include "pessoa.hpp"

class Contato : public Pessoa{
	private:
		string celular;
		string endereco;
	public:
		Contato();
		Contato(string celular,string endereco);
		string getCelular();
		void setCelular(string celular);
		string getEndereco();
		void setEndereco(string endereco);

};

#endif
