#ifndef AMIGO_H
#define AMIGO_H

#include "pessoa.hpp"

class Amigo : public Pessoa{
	private:
		string apelido;
		string email;
	public:
		Amigo();
		Amigo(string apelido,string email);
		string getApelido();
		void setApelido(string apelido);
		string getEmail();
		void setEmail(string email);

};

#endif
