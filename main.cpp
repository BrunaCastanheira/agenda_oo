#include <iostream>
#include "pessoa.cpp"
#include "amigo.cpp"
#include "contato.cpp"

using namespace std;


int main () {
	
	Pessoa x;
	Amigo y;
	Contato z;
	
	x.setNome ("Bruna");
	x.setIdade ("21");
	x.setTelefone ("1234-5678");
	
	y.setApelido ("Bru");
	y.setEmail ("anurb@email.com");
	
	z.setCelular ("8765-4321");
	z.setEndereco ("Guará");
 		
	cout << "Nome: " << x.getNome() << endl;
	cout << "Idade: " << x.getIdade() << endl;
	cout << "Telefone: " << x.getTelefone() << endl;
	cout << "Apelido: " << y.getApelido() << endl;
        cout << "Email: " << y.getEmail() << endl;
	cout << "Celular: " << z.getCelular() << endl;
        cout << "Endereco: " << z.getEndereco() << endl;

	
	return 0;
}
